//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var movimientosJSON = require('./movimientosv2');

var bodyparser = require('body-parser');
var requestjson = require('request-json');

var urlClientes = 'https://api.mlab.com/api/1/databases/jucan/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt'


app.use(bodyparser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/', (req, res) =>{
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.post("/", (req, res) => {
  res.send("Peticion recibida - [cambiada]");

});

app.delete('/', (req, res) =>{
  res.send('Hemos recibido su peticion DELETE');
})


app.get('/Clientes/:idcliente', (req, res) =>{
  res.send("Aqui tiene al cliente " + req.params.idcliente);
});

app.get('/v1/Movimientos', (req, res) =>{
  res.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos', (req, res) =>{
  res.json(movimientosJSON);
});


app.get('/v2/Movimientos/:id', (req, res) => {
  console.log( req.params.id );
  res.send( movimientosJSON[req.params.id - 1]);
});


app.get('/v2/MovimientosQuery', (req, res) => {
  console.log( req.query); // en la URL tiene que ser algo como ../v2/MovimientosQuery?param=Value&param2=ValueX&paramN=ValueN
  res.send('query recibido');
});



app.post('/v2/Movimientos', function(req, res){
  var nuevo = req.body;

  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);

  res.send("Movimiento dado de alta")
});

var clienteMLab = requestjson.createClient(urlClientes);

app.get('/Clientes', (req, res) =>{
  // body ==> la funcion puede retornar algo en el body
  clienteMLab.get('', function(err, resM, body){
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
    });
  });

  app.post('/Clientes', function(req, res){
    clienteMLab.post('', req.body, function(err, resM, body){
        res.send(body);
    });

  });
